package com.uvideoguidestatus.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.RequestConfiguration;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.uvideoguidestatus.R;
import com.uvideoguidestatus.uti.ConnectivityReceiver;
import com.uvideoguidestatus.uti.Constant;
import com.uvideoguidestatus.uti.Utils;
import com.uvideoguidestatus.uti.uvideo;

import java.util.Arrays;

public class InstructionActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener  {

    private String TAG="InstructionActivity";
    private AdView adView;

    private FrameLayout adContainerView;
    private FrameLayout adContainerbottomView;
    private boolean doubleBackToExit = false;

    private RelativeLayout rl_app;
    private RelativeLayout rl_appdwn;
    private RelativeLayout rl_status;
    private RelativeLayout rl_statusdwn;
    private RelativeLayout rl_appban;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instruction);
        init();
    }
    private void init()
    {
        adContainerView = findViewById(R.id.ad_view_container);

        adContainerbottomView = findViewById(R.id.ad_view_containers);

        // Initialize the Mobile Ads SDK.
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {}
        });

        // Set your test devices. Check your logcat output for the hashed device ID to
        // get test ads on a physical device. e.g.
        // "Use RequestConfiguration.Builder().setTestDeviceIds(Arrays.asList("ABCDEF012345"))
        // to get test ads on this device."
        MobileAds.setRequestConfiguration(
                new RequestConfiguration.Builder().setTestDeviceIds(Arrays.asList("ABCDEF012345")).build());

        adContainerView = findViewById(R.id.ad_view_container);

        adContainerbottomView = findViewById(R.id.ad_view_containers);

        // Since we're loading the banner based on the adContainerView size, we need to wait until this
        // view is laid out before we can get the width.
        adContainerView.post(new Runnable() {
            @Override
            public void run() {
                loadBanner();
            }
        });

        adContainerbottomView.post(new Runnable() {
            @Override
            public void run() {
                loadbottomBanner();
            }
        });

        rl_app=findViewById(R.id.rl_app);
        rl_appdwn=findViewById(R.id.rl_appdwn);
        rl_status=findViewById(R.id.rl_status);
        rl_statusdwn=findViewById(R.id.rl_statusdwn);
        rl_appban=findViewById(R.id.rl_appban);

        rl_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(InstructionActivity.this,InstructionDetailActivity.class);
                Utils.WriteSharePrefrence(InstructionActivity.this, Constant.KEY,"1");
                startActivity(intent);

            }
        });

        rl_appdwn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(InstructionActivity.this,InstructionDetailActivity.class);
                Utils.WriteSharePrefrence(InstructionActivity.this, Constant.KEY,"2");
                startActivity(intent);
            }
        });

        rl_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(InstructionActivity.this,InstructionDetailActivity.class);
                Utils.WriteSharePrefrence(InstructionActivity.this, Constant.KEY,"3");
                startActivity(intent);

            }
        });

        rl_statusdwn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(InstructionActivity.this,InstructionDetailActivity.class);
                Utils.WriteSharePrefrence(InstructionActivity.this, Constant.KEY,"4");
                startActivity(intent);
            }
        });

        rl_appban.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(InstructionActivity.this,InstructionDetailActivity.class);
                Utils.WriteSharePrefrence(InstructionActivity.this, Constant.KEY,"5");
                startActivity(intent);
            }
        });



    }

    /** Called when leaving the activity */
    @Override
    public void onPause() {
        if (adView != null) {
            adView.pause();
        }
        super.onPause();
    }

    /** Called when returning to the activity */
    @Override
    public void onResume() {
        super.onResume();
        uvideo.getInstance().setConnectivityListener(this);

        if (adView != null) {
            adView.resume();
        }
    }

    /** Called before the activity is destroyed */
    @Override
    public void onDestroy() {
        if (adView != null) {
            adView.destroy();
        }
        super.onDestroy();
    }

    private void loadBanner() {
        // Create an ad request.
        adView = new AdView(this);
        adView.setAdUnitId(getString(R.string.ad_baner_id));
        adContainerView.removeAllViews();
        adContainerView.addView(adView);

        AdSize adSize = getAdSize();
        adView.setAdSize(adSize);

        AdRequest adRequest = new AdRequest.Builder().build();

        // Start loading the ad in the background.
        adView.loadAd(adRequest);
    }



    private AdSize getbottomAdSize() {
        // Determine the screen width (less decorations) to use for the ad width.
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float density = outMetrics.density;

        float adWidthPixels = adContainerbottomView.getWidth();

        // If the ad hasn't been laid out, default to the full screen width.
        if (adWidthPixels == 0) {
            adWidthPixels = outMetrics.widthPixels;
        }

        int adWidth = (int) (adWidthPixels / density);

        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(getApplicationContext(), adWidth);
    }
    ////


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void loadbottomBanner() {
        // Create an ad request.
        adView = new AdView(this);
        adView.setAdUnitId(getString(R.string.ad_baner_id));
        adContainerbottomView.removeAllViews();
        adContainerbottomView.addView(adView);

        AdSize adSize = getbottomAdSize();
        adView.setAdSize(adSize);

        AdRequest adRequest = new AdRequest.Builder().build();

        // Start loading the ad in the background.
        adView.loadAd(adRequest);
    }



    private AdSize getAdSize() {
        // Determine the screen width (less decorations) to use for the ad width.
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float density = outMetrics.density;

        float adWidthPixels = adContainerbottomView.getWidth();

        // If the ad hasn't been laid out, default to the full screen width.
        if (adWidthPixels == 0) {
            adWidthPixels = outMetrics.widthPixels;
        }

        int adWidth = (int) (adWidthPixels / density);

        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(getApplicationContext(), adWidth);
    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
}