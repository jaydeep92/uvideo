package com.uvideoguidestatus.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.uvideoguidestatus.R;
import com.uvideoguidestatus.uti.ConnectivityReceiver;
import com.uvideoguidestatus.uti.uvideo;

public class MainActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener  {

    private String TAG="MainActivity";
    private ImageView ic_start;
    private TextView iv_icbrser;
    private InterstitialAd mInterstitialAd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init()
    {
        mInterstitialAd = new InterstitialAd(getApplicationContext());
        //test ad
        //mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        //ive ads
        mInterstitialAd.setAdUnitId("ca-app-pub-9762662687323163/7176077087");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                Log.e("Ads", "onAdLoaded");
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {
                    Log.e("TAG", "The interstitial wasn't loaded yet.");
                }
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.e("Ads", "onAdFailedToLoad");
            }

            @Override
            public void onAdOpened() {
                Log.e("Ads", "onAdOpened");
            }

            @Override
            public void onAdLeftApplication() {
                Log.e("Ads", "onAdLeftApplication");
            }

            @Override
            public void onAdClosed() {

            }
        });

        //define and cickevent
        ic_start=findViewById(R.id.ic_startapp);
        iv_icbrser=findViewById(R.id.ic_icbrowser);

        ic_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mInterstitialAd = new InterstitialAd(getApplicationContext());
                //test ad
                //mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
                //ive ads
                mInterstitialAd.setAdUnitId("ca-app-pub-9762662687323163/7176077087");
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
                mInterstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        Log.e("Ads", "onAdLoaded");
                        if (mInterstitialAd.isLoaded()) {
                            mInterstitialAd.show();
                        } else {
                            Log.e("TAG", "The interstitial wasn't loaded yet.");
                        }
                    }

                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        Log.e("Ads", "onAdFailedToLoad");
                    }

                    @Override
                    public void onAdOpened() {
                        Log.e("Ads", "onAdOpened");
                    }

                    @Override
                    public void onAdLeftApplication() {
                        Log.e("Ads", "onAdLeftApplication");
                    }

                    @Override
                    public void onAdClosed() {

                    }
                });

                Intent intent=new Intent(MainActivity.this,InstructionActivity.class);
                startActivity(intent);
                finish();


            }
        });

        iv_icbrser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.localpe")));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.localpe")));
                }
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        uvideo.getInstance().setConnectivityListener(this);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
}