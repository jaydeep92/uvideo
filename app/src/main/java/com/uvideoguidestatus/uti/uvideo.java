package com.uvideoguidestatus.uti;

import android.content.Context;

import com.onesignal.OneSignal;
import com.uvideoguidestatus.firebase.MyNotificationOpenedHandler;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

public class uvideo extends MultiDexApplication {

    private static uvideo mApp;
    public static Context context;

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


    @Override
    public void onCreate() {
        super.onCreate();
        mApp = this;
        context = getApplicationContext();

        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .setNotificationOpenedHandler(new MyNotificationOpenedHandler(context))
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();

        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);

    }

    public static synchronized uvideo getInstance() {
        return mApp;
    }

    public static Context getContext() {
        return context;
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }





}